package com.constgy.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.constgy.absDb.AbstractDb;
import com.constgy.bl.Config;

public class ConstgyDb extends AbstractDb {
	
	@Override
	/**
	 * Get List of tables
	 */
	public List<String> getTables() {
		Connection c = this.getConnection();
		Statement stmt = null;
		List<String> list = new ArrayList<String>();
		try {

			stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery(Config.SqlTables);
			
			while (rs.next()) {
				list.add(rs.getString(Config.SelectChampTablesName));
			}
			rs.close();
			stmt.close();
			c.close();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return list;
	}

	@Override
	/**
	 * Get list of column
	 */
	public List<String> getColumns(String table) {
		
		Connection c = this.getConnection();
		Statement stmt = null;
		List<String> list = new ArrayList<String>();
		try {

			stmt = c.createStatement();
			String sql = Config.SqlColumns.replace("?", table);
			ResultSet rs = stmt.executeQuery(sql);
			
			while (rs.next()) {
				list.add(rs.getString(Config.SelectChampColumnsName));
			}
			rs.close();
			stmt.close();
			c.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public Connection getConnection() {
		Connection c = null;
		try {
			Class.forName(Config.driver);
			c = DriverManager.getConnection(Config.url, Config.userName,
					Config.password);
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
		return c;
	}

}
