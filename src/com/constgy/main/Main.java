package com.constgy.main;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.constgy.bl.Config;
import com.constgy.bl.ConstgyLogic;
import com.constgy.db.ConstgyDb;

public class Main {
	/**
	 * @param args
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws IOException, InterruptedException {
		if (args.length==0){
			System.err.println("Error : \n Please used one of the folowing parameters : all or TableName");
		}
		
		System.out.println("Load Configuration ...");
		Config.load("Conf.properties");
		
		ConstgyLogic cL = new ConstgyLogic();
		ConstgyDb constgyDb = new ConstgyDb();
		List<String> listTables = null;
		String tableName = args[0];
		if(tableName.equals("all")){
			System.out.println("Get Tables ...");
			listTables= constgyDb.getTables();
			System.out.println("Tables count : " + listTables.size());
		}else{
			listTables = new ArrayList<String>();
			for (String arg : args) {
				listTables.add(arg);
			}		
		}
		
		FileWriter writer = null;
		writer = new FileWriter("Constgy.txt",false);
		System.out.println("Start generation const ...");
		for (String string : listTables) {
			List<String> list1 =cL.generate(string);
			for (String string2 : list1) {
				System.out.println(string2);
				writer.write(string2 + "\n");
				Thread.sleep(100);
			}
		}
		writer.close();
		System.out.println("END ... See Constgy.txt file ");
	}

}
