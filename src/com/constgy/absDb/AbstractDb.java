/**
 * 
 */
package com.constgy.absDb;

import java.sql.Connection;
import java.util.List;

/**
 * @author mldiallo
 *
 */
public abstract class AbstractDb {

	public abstract List<String>  getTables();
	public abstract List<String> getColumns(String table);
	public abstract Connection getConnection();
}
