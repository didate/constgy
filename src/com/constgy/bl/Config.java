package com.constgy.bl;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class Config {

	public static String driver;
	public static String url;
	public static String userName;
	public static String password;
	public static String SqlTables;
	public static String SqlColumns;
	public static String SelectChampTablesName;
	public static String SelectChampColumnsName;
	public static String FormatConst;
	public static String CommentChar;

	public static void load(String fileName) throws IOException {
		Properties prop = getProperties(fileName);
		driver = prop.getProperty("Driver");
		url = prop.getProperty("Url");
		userName = prop.getProperty("UserName");
		password = prop.getProperty("Password");
		SqlTables = prop.getProperty("SqlTables");
		SqlColumns = prop.getProperty("SqlColumns");
		SelectChampTablesName = prop.getProperty("SelectChampTablesName");
		SelectChampColumnsName = prop.getProperty("SelectChampColumnsName");
		FormatConst = prop.getProperty("FormatConst");
		CommentChar = prop.getProperty("CommentChar");
	}

	private static Properties getProperties(String fileName) throws IOException {
		Properties properties = new Properties();
		FileInputStream input = new FileInputStream(fileName);
		try {
			properties.load(input);
			return properties;
		} finally {
			input.close();
		}
	}
}
