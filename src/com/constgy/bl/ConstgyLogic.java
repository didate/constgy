package com.constgy.bl;

import java.util.ArrayList;
import java.util.List;

import com.constgy.db.ConstgyDb;
import com.constgy.gl.EnumLangage;

public class ConstgyLogic {

	private ConstgyDb _ConstgyDb;
	public ConstgyLogic(){
		_ConstgyDb=new ConstgyDb();
	}
	public List<String> generate(String table){
		List<String> list = new ArrayList<String>();
		list.add(Config.CommentChar +table);
		List<String> columns = _ConstgyDb.getColumns(table);
		for (String iter : columns) {
			list.add(this.createConstant(iter));
		}
		return list;
	}
	
//	public void generate(EnumLangage langage){
//		_Langage=langage;
//		List<String> tables = _ConstgyDb.getTables();
//		for (String iter : tables) {
//			this.generate(langage, iter);
//		}
//	}
	
	private String createConstant(String column){
		return Config.FormatConst.replace("constValue", column) ;
	}
	
	
}
